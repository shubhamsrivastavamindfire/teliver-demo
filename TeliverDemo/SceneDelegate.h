//
//  SceneDelegate.h
//  TeliverDemo
//
//  Created by Hemant Sudhanshu on 11/07/23.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

