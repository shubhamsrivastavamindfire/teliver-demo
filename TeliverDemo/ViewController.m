//
//  ViewController.m
//  TeliverDemo
//
//  Created by Hemant Sudhanshu on 11/07/23.
//

#import "ViewController.h"
@import teliver;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)startTracking:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        TeliverTracker *tracker = [[TeliverTracker alloc]initForTrackingId:@"104" withCustomMessage:@"Pop Up Title/Message"];
        [Teliver startTrackingForUser:tracker withNavigationTitle:@"Navigation Bar Title"];
    });
}
@end
